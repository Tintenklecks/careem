//
//  CBTableViewController.swift
//  CBDemo
//
//  Created by Ingo on 17.07.18.
//  Copyright © 2018 Ingo. All rights reserved.
//

import UIKit
import Kingfisher
import PKHUD



// DEFINE NAME




class CATableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var infoLabel: UILabel!
    
    @IBOutlet var header: UIView!
    @IBOutlet weak var headerSearchBar: UISearchBar!
    @IBOutlet weak var movieTableView: UITableView!
    
    private let refresh = UIRefreshControl()
    
    private var searchTimer: Timer?
    @IBOutlet var searchTableView: UITableView!
    
    var darkView: UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerSearchBar.delegate = self
        
        self.movieTableView.rowHeight = UITableViewAutomaticDimension;
        self.movieTableView.estimatedRowHeight = 320;
        
        
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            movieTableView.refreshControl = refresh
        } else {
            movieTableView.addSubview(refresh)
        }
        refresh.addTarget(self, action: #selector(loadData), for: .valueChanged)
        self.view.backgroundColor = CI.mainColor
        self.headerSearchBar.tintColor = CI.mainColor
        self.header.backgroundColor = CI.mainColor
        self.infoView.backgroundColor = .white
        self.infoView.frame = self.view.bounds.insetBy(dx: 0, dy: header.bounds.size.height/2)
        loadData()
        
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide Navigation Bar whenever screen appears
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
        // Set Statusbar white
        UIApplication.shared.statusBarStyle = .lightContent
        
        // Remove top and bottom line
        self.headerSearchBar.backgroundImage = UIImage()
        
        // Set focus on search field
        self.headerSearchBar.becomeFirstResponder()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func loadData() {
        infoView.isHidden = false
        
        self.infoLabel.text = "Enter search term in the search field"
        
        Backend.getMovieResults(
            for: self.headerSearchBar.text,
            onSucess: {
                DispatchQueue.main.async{
                    
                    self.refresh.endRefreshing()
                    self.movieTableView.reloadData()
                    
                }
                
                
                
        }) { (backendError) in
            DispatchQueue.main.async{
                HUD.flash(.labeledError(title: "Error", subtitle: "\(backendError.description)\n\nwhile loading movie list"), delay: 2)
                self.refresh.endRefreshing()
                self.movieTableView.reloadData()
                
                
            }
        }
    }
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        // number of rows for the list of searches
        if tableView == self.searchTableView {
            return min(Settings.lastSearches.count, 10)
        }
        
        
        // number of rows for the list of movies for the search String
        let count = Backend.movieList.count
        self.view.bringSubview(toFront: infoView)
        infoView.isHidden = count != 0
        if let search = headerSearchBar.text,
            search != "" {
            self.infoLabel.text = "No movies available matching `\(search)`"
        } else {
            self.infoLabel.text = "Type a search string in the search field. The app searches for that content automatically\n\n" +
            "btw: I know you wanted a search button, but I think that it´s a more UX-friendly way. The little ▾ on the right brings up the last searches \n\nBut if wanted, I can switch back in a 10 min modification. Just gimme a ping"
        }
        if count > 0, let searchText = self.headerSearchBar.text {
            Settings.addSearch(searchText: searchText)
        }
        print ("COUNT: \(count)")
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // cell for the list of searches
        
        if tableView == self.searchTableView {
            let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
            cell.textLabel?.text = Settings.lastSearches[indexPath.row]
            cell.textLabel?.setFont(font: UIFont.systemFont(ofSize: 15, weight: .light))
            cell.textLabel?.adjustsFontSizeToFitWidth = true
            return cell
        }
        
        
        // movie cell
        if indexPath.row + 1 == Backend.movieList.count {
            Backend.getMovieResults(for: headerSearchBar.text, onSucess:  {
                DispatchQueue.main.async{
                    
                    self.movieTableView.reloadData()
                }
                
            })
            
        }
        
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MovieCell", for: indexPath) as? CBMovieCell,
            indexPath.row <  Backend.movieList.count
            else {
                return UITableViewCell(style: .default, reuseIdentifier: nil)
        }
        
        
        let movie = Backend.movieList[indexPath.row]
        
        
        cell.movieLabel.text = movie.title
        let ratingString = NSLocalizedString("Rating %@ by %@ votes", comment: "used in: Rating %@ by %d votes")
        cell.rating.text =  String(format: ratingString, movie.voteAverage.formattedValue(1), Double(movie.voteCount).formattedSpeed(0))
        
        if movie.overview.trimmingCharacters(in: .whitespaces) == "" {
            cell.summary.text = "no description available"
            cell.summary.textColor = UIColor.lightGray
        } else {
            cell.summary.text =  movie.overview
            cell.summary.textColor = UIColor.darkGray
        }
        
        cell.year.text = String(movie.releaseDate.prefix(4))
        
        cell.movieImageView.image = #imageLiteral(resourceName: "noimage")
        
        if let posterPath = movie.posterPath {
            
            let posterUrlString = "https://image.tmdb.org/t/p/w185" + posterPath
            if let posterUrl = URL(string: posterUrlString) {
                cell.movieImageView.kf.setImage(with: posterUrl  , placeholder: #imageLiteral(resourceName: "noimage"))
            }
        }
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.searchTableView {
            return 33
        } else {
            return 300
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.searchTableView {
            self.headerSearchBar.text = Settings.lastSearches[indexPath.row]
            self.hidePreviousSearches()
            self.loadData()

        }
    }
    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        self.view.endEditing(true)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? CADetailViewController {
            
            var row = 0
            
            if segue.identifier == "accessory",
                let cell = sender as? UITableViewCell,
                let indexPath = self.movieTableView.indexPath(for: cell) {
                row = indexPath.row
            } else if let selectedRow = movieTableView.indexPathForSelectedRow?.row {
                row = selectedRow
            }
            
            
            if row < Backend.movieList.count {
                
                vc.movieId = row
                
            }
        }
    }
    
    
    // MARK: - Searchbar Delegate -
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) { // called when text changes (including clear)
        
        if let timer = self.searchTimer {
            timer.invalidate()
            
        }
        
        self.searchTimer = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(getSearchResults), userInfo: nil, repeats: false)
        
        self.hidePreviousSearches()
        
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print ("Search")
    }
    
    func searchBarResultsListButtonClicked(_ searchBar: UISearchBar) {
        if Settings.lastSearches.count == 0 {
            HUD.flash(.label("There are no saved searches"), delay: 2)
        } else {
            if darkView == nil {
            self.showPreviousSearches()
            } else {
                self.hidePreviousSearches()
            }
            
            
        }
        
    }
    
    
    // MARK: - Auto Search Delay -
    
    var lastSearch: String = ""
    
    @objc func setSearchText( notification: NSNotification) {
        print(notification.userInfo as Any)
    }
    
    
    @objc func getSearchResults() {
        print ("******* REQUEST")
        
        
        if let timer = self.searchTimer {
            timer.invalidate() // Stop any timer event
        }
        
        guard let searchText = self.headerSearchBar.text else {
            return
        }
        
        if  searchText == "" {
            Backend.clearMovieList()
            self.movieTableView.reloadData()
            return
        }
        
        if searchText == self.lastSearch {
            return
        }

        
        
        Backend.getMovieResults(for: searchText, onSucess:  {
            DispatchQueue.main.async{
                self.lastSearch = searchText
                self.movieTableView.reloadData()
                
            }
            
        }, onError: {
            error in
            DispatchQueue.main.async{
                HUD.flash(.labeledError(title: NSLocalizedString("Error", comment: "used in: Error"), subtitle: error.description), delay: 2)
            }
        })
    }
    
    
    // List of last successful searches
    
    func showPreviousSearches(){
        print (Settings.lastSearches)
        if darkView != nil {
            return
        }
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(hidePreviousSearches))
        darkView = UIView(frame: movieTableView.frame)
        darkView?.addGestureRecognizer(gesture)
        darkView?.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        self.view.addSubview(darkView!)
        darkView?.alpha = 0
        self.view.addSubview(searchTableView)
        var frame = movieTableView.frame.insetBy(dx: 16, dy: 0)
        frame.size.height = 0
        searchTableView.frame = frame
        frame.size.height = movieTableView.frame.size.height / 2
        UIView.animate(withDuration: 0.4) {
            self.searchTableView.frame = frame
            self.darkView?.alpha = 1
        }
        searchTableView.reloadData()
        
    }
    
    
    @objc func hidePreviousSearches(){
        if self.darkView != nil {
            var frame = self.searchTableView.frame
            frame.size.height = 0
            UIView.animate(withDuration: 0.3, delay: 0, options: [], animations: {
                self.searchTableView.frame = frame
                self.darkView?.alpha = 0
            }) { (_) in
                self.searchTableView.removeFromSuperview()
                self.darkView?.removeFromSuperview()
                self.darkView = nil
            }
            
        }
    }
}

// MARK: - Movie Tableview Cell -

class CBMovieCell: UITableViewCell {
    @IBOutlet weak var movieLabel: UILabel!
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var summary: UILabel!
    @IBOutlet weak var rating: UILabel!
    @IBOutlet weak var year: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}



