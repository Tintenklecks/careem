//
//  CBDetailViewController.swift
//  CBDemo
//
//  Created by Ingo on 18.07.18.
//  Copyright © 2018 Ingo. All rights reserved.
//

import UIKit
import Kingfisher

class CADetailViewController: UIViewController {
    
    @IBOutlet weak var votingView: UIView!
    @IBOutlet weak var votingResultLabel: UILabel!
    @IBOutlet weak var votingAmountLabel: UILabel!

    @IBOutlet weak var additionalInfoLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!

    
    @IBOutlet weak var articleContent: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var originalTitle: UILabel!
    
    @IBOutlet weak var movieImage: UIImageView!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    
    var isLoaded = false
    
    var movieId: Int {
        set {
            movieDetail = Backend.movieList[newValue]
        }
        get {
            return -1 // DUMMY
        }
    }
    var movieDetail : MovieResult? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.contentSize = articleContent.bounds.size
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        votingView.layer.cornerRadius = votingView.bounds.size.width / 2
        votingView.rotate(angle: 15)
        votingView.layer.shadowColor = UIColor.black.cgColor
        votingView.layer.shadowOffset = CGSize(width: 3, height: 3)
        votingView.layer.shadowRadius = 4
        votingView.clipsToBounds = false
        isLoaded = true
        reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reloadData() {
        
        
        guard isLoaded, let movie = self.movieDetail else {
            return
        }
        
        
        movieTitle.text = movie.title
        if movie.title != movie.originalTitle {
            originalTitle.text = "Original title: \(movie.originalTitle)"
        } else {
            originalTitle.text = ""
        }
        
        yearLabel.text = String(movie.releaseDate.prefix(4))

        votingResultLabel.text = movie.voteAverage.formattedValue(1)
        votingAmountLabel.text = "\(movie.voteCount) votes"
        additionalInfoLabel.text =
            "Original language: \(movie.originalLanguage) \n" +
            "Popularity: \(movie.popularity) \n" +
        ""
        descriptionLabel.text = movie.overview

        
        // The image is pre-set with a dummy
        // after it´s downloaded, we calculate the right scale for the real size and adjust the height constraint
        if let posterPath = movie.posterPath {
            
            let posterUrlString = "https://image.tmdb.org/t/p/w780" + posterPath
            if let posterUrl = URL(string: posterUrlString) {
                movieImage.kf.setImage(with: posterUrl, placeholder: #imageLiteral(resourceName: "noimage"), options: nil, progressBlock: nil) { (image, progress, _, _) in
                    if let image = image,
                        image.size.height > 0 {
                        let scale = image.size.width / image.size.height
                        let newHeight = self.view.bounds.size.height * scale
                        self.imageHeightConstraint.constant = newHeight
                    }
                    
                    
                }
            }
        }
        
        
        
        
      
        
    }
    
}


