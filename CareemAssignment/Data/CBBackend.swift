//
//  CBBackend.swift
//  CBDemo
//
//  Created by Ingo on 17.07.18.
//  Copyright © 2018 Ingo. All rights reserved.
//

import Foundation


enum BackendError: Int {
    case no = 0
    case url
    case noResponse
    case badResponse
    case badData
    case badJSON
    case badStructure
    case badSearchString


    
    var description: String {
        switch self {
            
        case .no:
            return NSLocalizedString("No error occured", comment: "Error Code Description")
        case .url:
            return NSLocalizedString("Improper Backend Error", comment: "Error Code Description")
            
        case .noResponse:
            return NSLocalizedString("No Response from the Server", comment: "Error Code Description")
        case .badResponse:
            return NSLocalizedString("Server Error", comment: "Error Code Description")
        case .badData:
            return NSLocalizedString("Bad Data result", comment: "Error Code Description")

        case .badJSON:
            return NSLocalizedString("Malformed JSON result", comment: "Error Code Description")

        case .badStructure:
            return NSLocalizedString("Wrong Data Structure", comment: "Error Code Description")

        case .badSearchString:
            return NSLocalizedString("Bad character in search string", comment: "Error Code Description")

        }
    }
}

  

class Backend {
    
    static var movieList: [MovieResult] = [] // List of all results til now
    static var totalResults: Int?
    static var totalPages: Int?
    static var lastPageLoaded = 0
    static var lastSearch = ""

    static func clearMovieList () {
        movieList = []
    }
    
    static func getMovieResults(for searchString: String?, onSucess: @escaping () -> Void,
                        onError: @escaping (_ errorCode: BackendError) -> Void = { errorNo in print(errorNo) } )  {
        
        func resetMovieList() {
            Backend.movieList = []
            Backend.lastPageLoaded = 1
            Backend.totalResults = nil
            Backend.totalPages = nil
        }
        
        guard let search = searchString   else {
            onSucess()
            resetMovieList()
            return
        }
        
        guard search != "" else {
            onSucess()
            resetMovieList()
            return
        }
        

        
        if  search != Backend.lastSearch { // New search
            resetMovieList()
        } else {
            Backend.lastPageLoaded += 1
        }
        
        
        if let totalPages = Backend.totalPages, totalPages < lastPageLoaded {
            //Do nothing - not even a refresh
            return
        }
        
        
        guard let urlSearchString = search.addingPercentEncoding(withAllowedCharacters: .alphanumerics) else {
            onError(.badSearchString)
            return
        }
        
        let urlString = "https://api.themoviedb.org/3/search/movie?api_key=2696829a81b1b5827d515ff121700838&query=\(urlSearchString)&page=\(Backend.lastPageLoaded)"

        guard let url = URL(string:  urlString) else {
            // ERROR URL NOT OK
            onError(.url)
            return
        }
        var request = URLRequest(url: url)
        request.timeoutInterval = 5

        request.httpMethod = "GET"
        request.cachePolicy = .reloadIgnoringLocalCacheData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        let task = URLSession.shared.dataTask(with: request) {
            data,response,error in
            
            guard let response = response as? HTTPURLResponse else {
                // ERROR CASE NO RESPONSE
                onError(.noResponse)
                return
            }
            let responseCode = response.statusCode
            
            guard responseCode == 200  else {
                // ERROR CASE BAD RESPONSE
                onError(.badResponse)
                return
            }
            
            
            guard let data = data else {
                    // ERROR CASE BAD DATA
                    onError(.badData)
                    return
            }
            
            
            do {
                let movies = try JSONDecoder().decode(MovieResults.self, from: data)
                Backend.movieList.append(contentsOf: movies.results) 
                Backend.totalPages = movies.totalPages
                Backend.totalResults = movies.totalResults
                Backend.lastSearch = search

                onSucess()
                
            } catch let error as NSError {
                print(error.localizedDescription)
                onError(.badStructure)
            }
            
            
        }
        task.resume()
        
        
    }


}
