//
//  SettingsManager.swift
//  SwiftSettingsManager
//

import Foundation
import UIKit

//var standardStorage = UserDefaults.standard

class Settings {
    
    static let shared = Settings()
    
    // ***************** Default Settings GETTER
    
    // Integer Value
    
    class func getInt(_ key: String, defaultValue: Int = 0) -> Int {
        if UserDefaults.standard.object(forKey: key) != nil {
            let returnValue = UserDefaults.standard.object(forKey: key) as! Int
            return returnValue
        } else {
            return defaultValue // default value
        }
    }
    
    // Double Value
    
    class func getDouble(_ key: String, defaultValue: Double = 0.0) -> Double {
        if UserDefaults.standard.object(forKey: key) != nil {
            let returnValue = UserDefaults.standard.object(forKey: key) as! Double
            return returnValue
        } else {
            return defaultValue // default value
        }
    }
    
    // Bool Value
    
    class func getBool(_ key: String, defaultValue: Bool = false) -> Bool {
        if UserDefaults.standard.object(forKey: key) != nil {
            let returnValue = UserDefaults.standard.object(forKey: key) as! Bool
            return returnValue
        } else {
            return defaultValue // default value
        }
    }
    
    // String Value
    
    class func getString(_ key: String, defaultValue: String = "") -> String {
        if UserDefaults.standard.object(forKey: key) != nil {
            let returnValue = UserDefaults.standard.object(forKey: key) as! String
            return returnValue
        } else {
            return defaultValue // default value
        }
    }
    
    // ***************** SETTER User Defaults Values
    
    class func setInt(_ key: String, value: Int) {
        UserDefaults.standard.set(value, forKey: key)
    }
    
    class func setDouble(_ key: String, value: Double) {
        UserDefaults.standard.set(value, forKey: key)
    }
    
    class func setBool(_ key: String, value: Bool) {
        UserDefaults.standard.set(value, forKey: key)
    }
    
    class func setString(_ key: String, value: String) {
        UserDefaults.standard.set(value, forKey: key)
    }
    
    
    class func getDate(_ key: String, defaultValue: Date = Date.distantPast) -> Date {
        if UserDefaults.standard.object(forKey: key) != nil,
            let returnValue = UserDefaults.standard.object(forKey: key) as? Date {
            return returnValue
        } else {
            return defaultValue // default value
        }
    }

    class func setDate(_ key: String, value: Date) {
        UserDefaults.standard.set(value, forKey: key)
    }
    

    static func addSearch(searchText: String) {
        var lastSearches = Settings.lastSearches
        for index in (0..<lastSearches.count).reversed() {
            let searchListItem = lastSearches[index].lowercased()
            let searchTextPrefix = searchText.lowercased().prefix(searchListItem.count)
            let searchListItemPrefix = searchListItem.prefix(searchText.count)
            
            
            if searchListItem  == searchTextPrefix { // if the current entry is the prefix to the searchText , remove entry and set searchText at the end of the list
                lastSearches.remove(at: index)
                break
            } else if searchText.lowercased() == searchListItemPrefix {
                // 
            }
        }
        lastSearches.append(searchText)
        Settings.lastSearches = lastSearches // save list to User Settings
    }
    
    
    
    static var lastSearches: [String] {
        get {
            if let returnValue = UserDefaults.standard.object(forKey: "lastSearches") as? [String] {
                return returnValue.reversed()
            }
            return []
            
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "lastSearches")
        }
    }
    
    

    
}
