//
//  Extensions.swift
//
//  Created by Ingo on 28.04.16.
//

import Foundation
import UIKit
import CoreLocation

// MARK: Globals


var isMetric: Bool {
get {
    if #available(iOS 10.0, *) {
        return Locale.current.usesMetricSystem
        
    } else {
        
        if (Locale.current as NSLocale).object(forKey: .usesMetricSystem)  != nil {
            return true
        }
        return false
        
        
        
    }
}
}



let speedFactor = isMetric ? 3.6 : 2.2369
let distanceFactorSmall = isMetric ? 1 : 3.2808
let distanceFactorBig = isMetric ? 0.001 : 0.000621
let distanceSmallBorder = 10000.0


// MARK: - UIApplication extension -

extension UIView {
    func setFont(font: UIFont, recursive: Bool = true ) {
        for view in self.subviews {
            if view.subviews.count > 0 {
                view.setFont(font: font, recursive: recursive)
            }
            if view is UILabel {
                let label = view as! UILabel
                label.font = font
            }
        }
    }
}

extension UIApplication {
	func isInBackground() -> Bool {
		if self.applicationState != .active {
			return true
		} else {
			return false
		}
	}
    
    func versionString() -> String {
        
        if let dict = Bundle.main.infoDictionary {
            if let version = dict["CFBundleShortVersionString"] as? String
                , let bundleVersion = dict["CFBundleVersion"] as? String {
                return "v\(version) (\(bundleVersion))"
            }
        }
        return ""
    }

    
}


// MARK: - UIViewController -
extension UIViewController {
    
    func alert(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: NSLocalizedString("OK", comment: "used in: OK"), style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
}


// MARK: - INT64 = Int starting at iPhone 5s extension -

extension Int64 {
	func dateFromMillisecond() -> Date {
		let timeinterval = Double(self) / 1000.0
		return Date(timeIntervalSinceReferenceDate: timeinterval)
	}
}

// MARK: - DOUBLE extension -

extension Double {
	func speedUnit() -> String {
		if isMetric {
			return "km/h"
		} else {
			return "mph"
		}
	}

	func formattedValue(_ digits: Int = -1) -> String {
		let formatter: NumberFormatter = NumberFormatter()
		formatter.numberStyle = .decimal

		if digits < 0 {
		} else {
			formatter.minimumFractionDigits = digits
			formatter.maximumFractionDigits = digits
		}
		formatter.locale = Locale.current
		return formatter.string(from: NSNumber(floatLiteral: self))!  // (from: NSNumber(self))!
	}

	func formattedSpeed(_ additionalDigits: Int = 0) -> String {
		if self < 0 {
			return " - "
		}

		let value = self * speedFactor

		let formatter: NumberFormatter = NumberFormatter()
		formatter.numberStyle = .decimal

		if value < 10 {
			formatter.minimumFractionDigits = 1 + additionalDigits
			formatter.maximumFractionDigits = 1 + additionalDigits
		} else {
			formatter.minimumFractionDigits = 0 + additionalDigits
			formatter.maximumFractionDigits = 0 + additionalDigits
		}
		formatter.locale = Locale.current
        return formatter.string(from: NSNumber(floatLiteral:value))!
	}

	func distanceUnit() -> String {
		if (self * distanceFactorSmall) < distanceSmallBorder {
			if isMetric {
				return "m"
			} else {
				return "ft"
			}
		} else {
			if isMetric {
				return "km"
			} else {
				return "mi"
			}
		}
	}

	func formattedDistance(_ allowZero: Bool = true) -> String {
		if self < 0 {
			return "-"
		} else if self == 0 && !allowZero {
			return "-"
		}

		let formatter: NumberFormatter = NumberFormatter()

		var value = self * distanceFactorSmall

		formatter.minimumFractionDigits = 0
		formatter.maximumFractionDigits = 0

		if value >= distanceSmallBorder {
			value = self * distanceFactorBig

			if value <= 100 {
				formatter.minimumFractionDigits = 1
				formatter.maximumFractionDigits = 1
			}
		}

		formatter.numberStyle = .decimal

		formatter.locale = Locale.current
        return formatter.string(from: NSNumber(floatLiteral: value))!
	}

	func timeString() -> String {
		var value: Int = Int(self)

		let sec = value % 60
		value = value / 60

		let min = value % 60
		let hour = value / 60

		return String(format: "%d:%02d:%02d", hour, min, sec)

	}
    
     static func random(from: Double = 0, to: Double = 1) -> Double {
        return (Double(arc4random()) / Double(UINT32_MAX) * (to-from)) + from
    }
}

// MARK: - DATA extension -
extension Data {
    func hexEncodedString() -> String {
        return map { String(format: "%02hhx", $0) }.joined()
    }
}


// MARK: - UICOLOR extension -

extension UIColor {
	convenience init(hexString: String) {
		let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
		let scanner = Scanner(string: hexString)

		if (hexString.hasPrefix("#")) {
			scanner.scanLocation = 1
		}

		var color: UInt32 = 0
		scanner.scanHexInt32(&color)

		let mask = 0x000000FF
		let r = Int(color >> 16) & mask
		let g = Int(color >> 8) & mask
		let b = Int(color) & mask

		let red = CGFloat(r) / 255.0
		let green = CGFloat(g) / 255.0
		let blue = CGFloat(b) / 255.0

		self.init(red: red, green: green, blue: blue, alpha: 1)
	}

	func toHexString() -> String {
		var r: CGFloat = 0
		var g: CGFloat = 0
		var b: CGFloat = 0
		var a: CGFloat = 0

		getRed(&r, green: &g, blue: &b, alpha: &a)

		let rgb: Int = (Int)(r * 255) << 16 | (Int)(g * 255) << 8 | (Int)(b * 255) << 0

		return String(format: "#%06x", rgb)
	}
}

// MARK: - UIImage extension -

extension UIImage {
    public enum UIImageContentMode {
        case scaleToFill, scaleAspectFit, scaleAspectFill
    }
    
    
    func scaleTo(size: CGSize) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        
        self.draw(in: CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height))
        
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result
    }
    
    func scaleToFit(size: CGSize) -> UIImage? {
        guard  self.size.width > size.width || self.size.height > size.height else {
            return self
        }
        
        let aspect = self.size.width / self.size.height
        if size.width / aspect <= size.height {
            return scaleTo(size: CGSize(width: size.width, height: size.width / aspect))
        } else {
            return scaleTo(size: CGSize(width: size.height * aspect, height: size.height))
        }
    }

    
    
    func resize(toSize: CGSize, contentMode: UIImageContentMode = .scaleAspectFit) -> UIImage? {
        let horizontalRatio = toSize.width / self.size.width;
        let verticalRatio = toSize.height / self.size.height;
        var ratio: CGFloat!
        
        switch contentMode {
        case .scaleToFill:
            ratio = 1
        case .scaleAspectFill:
            ratio = max(horizontalRatio, verticalRatio)
        case .scaleAspectFit:
            ratio = min(horizontalRatio, verticalRatio)
        }
        
        let rect = CGRect(x: 0, y: 0, width: size.width * ratio, height: size.height * ratio)
        
        // Fix for a colorspace / transparency issue that affects some types of
        // images. See here: http://vocaro.com/trevor/blog/2009/10/12/resize-a-uiimage-the-right-way/comment-page-2/#comment-39951
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: nil, width: Int(rect.size.width), height: Int(rect.size.height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)
        
        let transform = CGAffineTransform.identity
        
        // Rotate and/or flip the image if required by its orientation
        context?.concatenate(transform);
        
        // Set the quality level to use when rescaling
        context!.interpolationQuality = CGInterpolationQuality(rawValue: 3)!
        
        //CGContextSetInterpolationQuality(context, CGInterpolationQuality(kCGInterpolationHigh.value))
        
        // Draw into the context; this scales the image
        context?.draw(self.cgImage!, in: rect)
        
        // Get the resized image from the context and a UIImage
        let newImage = UIImage(cgImage: (context?.makeImage()!)!, scale: self.scale, orientation: self.imageOrientation)
        return newImage;
    }
    

    
//    func resizeToSize(size: CGSize, contentMode: UIImageContentMode = .scaleAspectFit) -> UIImage? {
//        
//        let horizontalRatio = size.width / self.size.width;
//        let verticalRatio = size.height / self.size.height;
//        var ratio: CGFloat!
//        
//        switch contentMode {
//        case .scaleToFill:
//            ratio = 1
//        case .scaleAspectFill:
//            ratio = max(horizontalRatio, verticalRatio)
//        case .scaleAspectFit:
//            ratio = min(horizontalRatio, verticalRatio)
//        }
//        
//        let frame = CGRect(x: 0, y: 0, width: self.size.width * ratio, height: self.size.height * ratio)
//        
//        
//        
//        
//        
//        // Begins an image context with the specified size
//        UIGraphicsBeginImageContextWithOptions(frame.size, false, 0.0);
//        // Draws the input image (self) in the specified size
//        
////        let frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
//        self.draw(in: frame)
//        // Gets an UIImage from the image context
//        let result = UIGraphicsGetImageFromCurrentImageContext()
//        // Ends the image context
//        UIGraphicsEndImageContext()
//        // Returns the final image, or NULL on error
//        return result
//    }
//    

    
    
    func colorize(with color: UIColor) -> UIImage? {
        let maskImage = cgImage!
        
        let width = size.width
        let height = size.height
        let bounds = CGRect(x: 0, y: 0, width: width, height: height)
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!
        
        context.clip(to: bounds, mask: maskImage)
        context.setFillColor(color.cgColor)
        context.fill(bounds)
        
        if let cgImage = context.makeImage() {
            let coloredImage = UIImage(cgImage: cgImage)
            return coloredImage
        } else {
            return nil
        }
    }
    
}


// MARK: - UIVIEW extension -
extension UIView {

	func blink(_ start: Bool) {
		if start {
			self.alpha = 1
			UIView.animate(withDuration: 1.6, delay: 0.3, options: [.repeat, .autoreverse], animations: { 
				self.alpha = 0
				}, completion: nil)
		}
		else {
			self.alpha = 1
			self.layer.removeAllAnimations()
		}
    }
    
     func screenshotImage() -> UIImage? {
        
        UIGraphicsBeginImageContext(self.bounds.size)
        if let context = UIGraphicsGetCurrentContext() {
            self.layer.render(in: context)
            let screenShot = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext()
            return screenShot
        } else {
            return nil
        }
    }
    
    func screenshotImageView() ->  UIImageView? {
        
        if let image = self.screenshotImage() {
            let frame = self.frame
            let imageView =  UIImageView(image: image)
            imageView.frame = frame
            return imageView
        } else {
            return nil
        }
    }
    
    func rotate(angle: CGFloat, clockwise: Bool = true) {
        let rad = angle / 180 * CGFloat.pi * (clockwise ? 1 : -1)
        self.transform = CGAffineTransform(rotationAngle: rad)
    }

    func rotate(radian: CGFloat, clockwise: Bool = true) {
        let rad = radian * (clockwise ? 1 : -1)
        self.transform = CGAffineTransform(rotationAngle: rad)
    
    }
    
    func clearBackground() {
        // To remove IB Colors for aligning
        self.backgroundColor = UIColor.clear
        for subView in self.subviews {
            subView.clearBackground()
        }
    }
    func addMotionEffect(factor: Double = 1) {
        let verticalMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
        verticalMotionEffect.minimumRelativeValue = (-10 * factor)
        verticalMotionEffect.maximumRelativeValue = (10 * factor)
        // Set horizontal effect
        let horizontalMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        horizontalMotionEffect.minimumRelativeValue = (-10 * factor)
        horizontalMotionEffect.maximumRelativeValue = (10 * factor)
        // Create group to combine both
        let group = UIMotionEffectGroup()
        group.motionEffects = [horizontalMotionEffect, verticalMotionEffect]
        // Add both effects to your view
        self.addMotionEffect(group)
    }
    
    func addParallaxMotionEffects(tiltValue : CGFloat = 0.25, panValue: CGFloat = 5) {
        var xTilt = UIInterpolatingMotionEffect()
        var yTilt = UIInterpolatingMotionEffect()
        var xPan = UIInterpolatingMotionEffect()
        var yPan = UIInterpolatingMotionEffect()
        let motionGroup = UIMotionEffectGroup()
        xTilt = UIInterpolatingMotionEffect(keyPath:
            "layer.transform.rotation.y", type: .tiltAlongHorizontalAxis)
        xTilt.minimumRelativeValue = -tiltValue
        xTilt.maximumRelativeValue = tiltValue
        yTilt = UIInterpolatingMotionEffect(keyPath:
            "layer.transform.rotation.x", type: .tiltAlongVerticalAxis)
        yTilt.minimumRelativeValue = -tiltValue
        yTilt.maximumRelativeValue = tiltValue
        xPan = UIInterpolatingMotionEffect(keyPath: "center.x", type:     .tiltAlongHorizontalAxis)
        xPan.minimumRelativeValue = -panValue
        xPan.maximumRelativeValue = panValue
        yPan = UIInterpolatingMotionEffect(keyPath: "center.y", type:    .tiltAlongVerticalAxis)
        yPan.minimumRelativeValue = -panValue
        yPan.maximumRelativeValue = panValue
        motionGroup.motionEffects = [xTilt, yTilt, xPan, yPan]
        self.addMotionEffect( motionGroup )
    }

}

// MARK: - NSDATE extension -

extension Date {

	func milliseconds () -> Int64 {
        
        let seconds = Double(self.timeIntervalSinceReferenceDate)
        
        
		let timestamp = seconds * 1000.0
		return Int64(timestamp)
	}

	func formattedFromCompenents(_ styleAttitude: DateFormatter.Style, year: Bool = true, month: Bool = true, day: Bool = true, hour: Bool = true, minute: Bool = true, second: Bool = true) -> String {
		let long = styleAttitude == .long || styleAttitude == .full ? true : false
		var comps = ""

		if year { comps += long ? "yyyy" : "yy" }
		if month { comps += long ? "MMM" : "MM" }
		if day { comps += long ? "dd" : "d" }

		if hour { comps += long ? "HH" : "H" }
		if minute { comps += long ? "mm" : "m" }
		if second { comps += long ? "ss" : "s" }

		let format = DateFormatter.dateFormat(fromTemplate: comps, options: 0, locale: Locale.current)
		let formatter = DateFormatter()
		formatter.dateFormat = format
		return formatter.string(from: self)
	}

	func indexPaneFormat() -> String {
		let comps = "MMdd"

		let format = DateFormatter.dateFormat(fromTemplate: comps, options: 0, locale: Locale.current)
		let formatter = DateFormatter()
		formatter.dateFormat = format
		return formatter.string(from: self)

	}

	func justTime(_ withSeconds: Bool) -> String {

		let dateFormatter: DateFormatter = DateFormatter()
		dateFormatter.dateStyle = .none
		dateFormatter.timeStyle = withSeconds ? .medium : .short
		dateFormatter.locale = Locale.current
		return dateFormatter.string(from: self)

	}

	func justDate(_ withYear: Bool) -> String {

		let dateFormatter: DateFormatter = DateFormatter()
		dateFormatter.dateStyle = withYear ? .medium : .short
		dateFormatter.timeStyle = .none
		dateFormatter.locale = Locale.current
		return dateFormatter.string(from: self)
	}

	func justDateAndTime(_ longStyle: Bool) -> String {

		let dateFormatter: DateFormatter = DateFormatter()
		dateFormatter.dateStyle = longStyle ? .medium : .short
		dateFormatter.timeStyle = longStyle ? .medium : .short
		dateFormatter.locale = Locale.current
		return dateFormatter.string(from: self)
	}

	func normalizedDateString() -> String {
		let dateFormatter: DateFormatter = DateFormatter()
		dateFormatter.timeZone = TimeZone.autoupdatingCurrent
		dateFormatter.dateFormat = "yyyyMMdd"

		return dateFormatter.string(from: self)

	}

	func csvDateString() -> String {
		let dateFormatter: DateFormatter = DateFormatter()
//        dateFormatter.timeZone = NSTimeZone.localTimeZone()
		dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
		dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss XXX"

		return dateFormatter.string(from: self)

	}


	func startOfDay() -> Date {
		var calendar = Calendar.current
		calendar.timeZone = TimeZone(secondsFromGMT: 0)!
		let midnight = calendar.startOfDay(for: self)
		return midnight
	}

	func endOfDay() -> Date {
		let oneDay: TimeInterval = 86400 // seconds per day

		var calendar = Calendar.current
		calendar.timeZone = TimeZone(secondsFromGMT: 0)!
		let startOfNextDay = calendar.startOfDay(for: self).addingTimeInterval(oneDay - 1)

		return startOfNextDay

	}

	func dateTimeFilename () -> String {

		let dateFormatter: DateFormatter = DateFormatter()
		dateFormatter.timeZone = TimeZone.autoupdatingCurrent
		dateFormatter.dateFormat = "yyyyMMdd-HHmmss"

		return dateFormatter.string(from: self)

	}

	static func dateFromString(_ stringDate: String) -> Date {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss XXX"
		if stringDate < "1000" {
			return Date.distantPast
		} else if stringDate > "3000" {
			return Date.distantFuture
		}
		// return NSDate()
		return dateFormatter.date(from: stringDate)!
	}
    
     static func daysBetween(date1 d1: Date, date2 d2: Date) -> Int {
        let dc = Calendar.current.dateComponents([.day], from: d1, to: d2)
        return dc.day!
    }
    


}

// MARK: - STRING extension (append to file ) -

extension String {
    
    func jsonToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
//                print(error.localizedDescription)
            }
        }
        return nil
    }

    

	func writeAndAppendString(toFile fileName: String) {

		let data: Data = (self + "\n").data(using: String.Encoding.utf8)!

		let url = URL.documentPathURLForFileNamed(fileName)

		if !(url as NSURL).checkResourceIsReachableAndReturnError(nil) {
			try! "".write(to: url, atomically: true, encoding: String.Encoding.utf8)
		}

		var fileHandle: FileHandle? = nil

		do {
			fileHandle = try FileHandle(forUpdating: url)

		} catch let error as NSError {
			print(error)
			return
		}

		fileHandle!.seekToEndOfFile()
		fileHandle!.write(data)
		fileHandle!.closeFile()

	}

	func dateFromCSVString() -> Date? {
		let rfc3339DateFormatter = DateFormatter()
		rfc3339DateFormatter.locale = Locale(identifier: "en_US_POSIX")
		rfc3339DateFormatter.dateFormat = "yyyy-MM-dd' 'HH:mm:ssZZZZZ"
		rfc3339DateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
		return rfc3339DateFormatter.date(from: self)

	}

	func dateFromNormalizedDateString() -> Date? {
		let normalizedDateFormatter = DateFormatter()
		normalizedDateFormatter.locale = Locale(identifier: "en_US_POSIX")
		normalizedDateFormatter.dateFormat = "yyyyMMdd"
		normalizedDateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
		return normalizedDateFormatter.date(from: self)
	}

}

extension String {
    func deleteHTMLTag(tag:String) -> String {
        return self.replacingOccurrences(of: "(?i)</?\(tag)\\b[^<]*>", with: "", options: .regularExpression, range: nil)
    }
    
    func deleteHTMLTags(tags:[String]) -> String {
        var mutableString = self
        for tag in tags {
            mutableString = mutableString.deleteHTMLTag(tag: tag)
        }
        return mutableString
    }
}


// MARK: CLLocation

extension CLLocation {
	func isMentionableDifferenceTo(_ location: CLLocation) -> Bool {

		return true
		// TODO:
//		if location.speed >= 0 && self.speed >= 0 {
//			if self.speed - location {
//				<#code#>
//			}
//		} else if location.speed < 0 && self.speed < 0 {
//
//		}

	}
}
// MARK: - NSFilemanager extension (append to file ) -

extension URL {
	static func documentPathURL () -> URL {
		let documentDirectoryURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
		return documentDirectoryURL
	}

	static func documentPathURLForFileNamed (_ filename: String) -> URL {
		let fileDestinationUrl = URL.documentPathURL().appendingPathComponent(filename)

		return fileDestinationUrl
	}
	static func libraryPathURL () -> URL {
		let documentDirectoryURL = try! FileManager.default.url(for: .libraryDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
		return documentDirectoryURL
	}

	static func libraryPathURLForFileNamed (_ filename: String) -> URL {
		let fileDestinationUrl = URL.libraryPathURL().appendingPathComponent(filename)

		return fileDestinationUrl
	}
    static func bundlePathURL () -> URL {
        
        return Bundle.main.bundleURL
    }
    
    static func bundlePathURLForFileNamed (_ filename: String) -> URL {
        let bundleFileUrl = URL.bundlePathURL().appendingPathComponent(filename)
        
        return bundleFileUrl
    }

}
extension Dictionary  {
    func jsonStringForREST() -> String? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self)
            if let jsonBody = String(data: jsonData, encoding: .utf8) {
                return jsonBody
            }
        } catch {
        }
         return nil
        

    }
    
    
    func queryString() -> String {
        let parameterArray = self.map { (key, value) -> String in
            let percentEscapedKey = (key as! String).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let percentEscapedValue = (value as! String).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            return "\(percentEscapedKey)=\(percentEscapedValue)"
        }
        
        return parameterArray.joined(separator: "&")
    }

}

extension NSLayoutConstraint {
    
    override open var description: String {
        if let id = identifier {
            return "id: \(id), constant: \(constant)" //you may print whatever you want here
        } else {
            return super.description
        }
    }
}

