//
//  CI.swift
//  CBDemo
//
//  Created by Ingo on 17.07.18.
//  Copyright © 2018 Ingo. All rights reserved.
//

import Foundation
import UIKit

class CI {
    static let mainColor: UIColor = #colorLiteral(red: 0.2078431373, green: 0.6745098039, blue: 0.2666666667, alpha: 1)
    static let logoColor: UIColor =   #colorLiteral(red: 0.2078431373, green: 0.6745098039, blue: 0.2666666667, alpha: 1)
    static let lightBackgroundColor = #colorLiteral(red: 0.9529411765, green: 0.968627451, blue: 0.9882352941, alpha: 1)
    static let lightForegroundColor = #colorLiteral(red: 0.06274509804, green: 0.06274509804, blue: 0.06274509804, alpha: 1)
}
