//
//  CareemAssignmentTests.swift
//  CareemAssignmentTests
//
//  Created by Ingo on 22.07.18.
//  Copyright © 2018 Ingo. All rights reserved.
//

import XCTest
@testable import CareemAssignment

class CareemAssignmentTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testMovieList() {
        let expectation: XCTestExpectation? = self.expectation(description: "Got Movie List")
        Backend.getMovieResults(for: "Forrest", onSucess: {
            if Backend.movieList.count > 0 {
                expectation?.fulfill()
            } else {
                XCTFail("No products available")
            }
            
        }, onError: { (errorCode) in
            XCTFail("Error \(errorCode)")
            
        })
        
        
        
        
        waitForExpectations(timeout: 5, handler: { error in
            if error != nil {
                if let aDescription = expectation?.description {
                    XCTFail("Error (\(aDescription))")
                }
            }
        })
    }
    
    
    func testIdioticSearch() {
        let expectation: XCTestExpectation? = self.expectation(description: "Got No Movie for stupid search")
        Backend.getMovieResults(for: "lsjdhebncg", onSucess: {
            if Backend.movieList.count > 0 {
                XCTFail("Products available though shouldn´t be")
            } else {
                expectation?.fulfill()
            }
            
        }, onError: { (errorCode) in
            XCTFail("Error \(errorCode)")            
        })
        
        
        
        
        waitForExpectations(timeout: 5, handler: { error in
            if error != nil {
                if let aDescription = expectation?.description {
                    XCTFail("Error (\(aDescription))")
                }
            }
        })
    }
    
    

    
    
}
